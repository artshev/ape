struct win32_offscreen_buffer  
{
    // NOTE(casey): Pixels are alwasy 32-bits wide, Memory Order BB GG RR XX
    BITMAPINFO Info;
    void *Memory;
    int Width;
    int Height;
    int Pitch;
};

struct win32_window_dimension
{
    int Width;
    int Height;
};

struct voice_info
{
    bool32 Playing;
    bool32 OneShot;
    int32 SampleIndex;
    int32 TotalBytes;
    real32 VolumeL;
    real32 VolumeR;
    real32 Volume;
    real32 Panning;
    
    int16 *FirstSample;
    int16 *Memory;
};

struct audio_buffer
{
    int32 SampleRate;
    int16 BytesPerSample;                
    int32 SecondaryBufferSize;
    uint32 RunningSampleIndex;
    int32 LatencySampleCount;
    
    real32 MasterVolume;
    bool32 MasterPlaying;
    
    voice_info Voices[4];
};
 
struct WAVE_header
{
    int32 ChunkID;
    int32 ChunkSize;
    int32 Format;
};

struct WAVE_fmt
{
    int32 ChunkID;
    int32 ChunkIDSize;
    int16 AudioFormat;
    int16 NumChannels;       
    int32 SampleRate;
    int32 ByteRate;
    int16 BlockAlign;
    int16 BitsPerSample;
};

struct WAVE_data
{
    int32 ChunkID;
    int32 ChunkSize;
    void *data;
};   

struct rgb_color
{
    uint8 Red;
    uint8 Green;
    uint8 Blue;
};
