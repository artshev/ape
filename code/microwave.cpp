#include <windows.h>
#include <stdint.h>
#include <xinput.h>
#include <dsound.h>
#include <stdio.h> 
#include <assert.h>
#include <math.h>

#define internal static 
#define local_persist static 
#define global_variable static

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef int32 bool32;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef float real32;
typedef double real64;

#define PI 3.14159265f

#include "vector.h"
#include "microwave.h"

global_variable real32 ScreenCenterX = 640.0f;
global_variable real32 ScreenCenterY = 360.0f;
global_variable bool GlobalRunning;
global_variable win32_offscreen_buffer GlobalBackbuffer;
global_variable LPDIRECTSOUNDBUFFER SecondaryBuffer;

internal win32_window_dimension
Win32GetWindowDimension(HWND Window)
{
    win32_window_dimension Result;
    
    RECT ClientRect;
    GetClientRect(Window, &ClientRect);
    Result.Width = ClientRect.right - ClientRect.left;
    Result.Height = ClientRect.bottom - ClientRect.top;
    
    return(Result);
}

internal real32
Round(real32 Val)
{
    return (int32)(Val += 0.5f);
}


inline real32
RadDeg(real32 Val)
{
    return 360/(2*PI) * Val;
}


internal void
ClearScreen(win32_offscreen_buffer *Buffer, rgb_color Color)
{
    uint8 *Row = (uint8 *)Buffer->Memory;    
    for(int Y = 0;
        Y < Buffer->Height;
        ++Y)
    {
        uint32 *Pixel = (uint32 *)Row;
        for(int X = 0;
            X < Buffer->Width;
            ++X)
        {
            uint8 Blue = Color.Blue;
            uint8 Green = Color.Green;
            uint8 Red = Color.Red;
            
            *Pixel++ = (Red << 16| (Green << 8) | (Blue << 0)); 
        }
        
        Row += Buffer->Pitch;
    }
}

internal void
DrawPoint(win32_offscreen_buffer *Buffer, rgb_color Color, v2 Position)
{
    int32_v2 Point;
    Point.x = (int32)Position.x;
    Point.y = (int32)Position.y;
   
    int32 BytesPerPixel = 4;
    uint8 *Row = (uint8 *)Buffer->Memory;
    
    Row += (Buffer->Pitch * Point.y) + (Point.x * BytesPerPixel);
    
    uint32 *Pixel = (uint32 *)Row;
    *Pixel = (Color.Red << 16| (Color.Green << 8) | (Color.Blue << 0));
}

struct point
{
    v2 Position;
    real32 Angle;
};

internal void
DrawLine(win32_offscreen_buffer *Buffer, rgb_color Color, v2 PointA, v2 PointB)
{
    v2 TargetPixel = PointA;
    v2 Line = Sub(PointA, PointB);
    v2 LineHat = Unit(Line);
    point Points[4] = {0};
    
    int32 Counter = 0;
    while(Counter < 100)
    {
        DrawPoint(Buffer, Color, TargetPixel);
               
        v2 Between;
        Points[0].Position = {TargetPixel.x - 1, TargetPixel.y};
        Between = Sub({TargetPixel.x - 1, TargetPixel.y}, PointA);
        Points[0].Angle = Dot(Between, LineHat);
               
        Points[1].Position = {TargetPixel.x + 1, TargetPixel.y};
        Between = Sub({TargetPixel.x + 1, TargetPixel.y}, PointA);
        Points[1].Angle = Dot(Between, LineHat);
               
        Points[2].Position = {TargetPixel.x, TargetPixel.y - 1};
        Between = Sub({TargetPixel.x, TargetPixel.y - 1}, PointA);
        Points[2].Angle = Dot(Between, LineHat);
               
        Points[3].Position = {TargetPixel.x, TargetPixel.y + 1};
        Between = Sub({TargetPixel.x, TargetPixel.y + 1}, PointA);
        Points[3].Angle = Dot(Between, LineHat);
        
        point Temp;
        bool32 Swapped = false;
        for(int i = 0;
            i < 3;
            i++)
        {
            Swapped = false;
            for(int j = 0;
                j < 3-i;
                j++)
            {
                if(Points[j].Angle > Points[j+1].Angle)
                {
                    Temp = Points[j];
                    Points[j] = Points[j+1];
                    Points[j+1] = Temp;
                    
                    Swapped = true;
                }
            }
            if(!Swapped)
            {
                break;
            }
        }
         
        TargetPixel = Points[0].Position;
        Counter++;
    }
}

internal void
DrawRect(win32_offscreen_buffer *Buffer, rgb_color Color, int32 Width, v2 Vec)
{
    int32_v2 Position;
    int32 Shift = Width/2;
    // shuld round these?
    Position.x = (int32)Round(Vec.x - Shift);
    Position.y = (int32)Round(Vec.y - Shift);
        
    uint8 *Row = (uint8 *)Buffer->Memory;    
    for(int Y = 0;
        Y < Buffer->Height;
        ++Y)
    {
        uint32 *Pixel = (uint32 *)Row;
        for(int X = 0;
            X < Buffer->Width;
            ++X)
        {  
            uint8 Blue = Color.Blue;
            uint8 Green = Color.Green;
            uint8 Red = Color.Red; 
            
            if( (Y > Position.y) && (Y < (Position.y + Width)) )
            {
                if((X > Position.x) && (X < (Position.x + Width)) )
                {
                    *Pixel++ = (Red << 16| (Green << 8) | (Blue << 0));
                }
                else
                {
                    Pixel++;
                }
            }
        }
        
        Row += Buffer->Pitch;
    }
}

internal void
Win32ResizeDIBSection(win32_offscreen_buffer *Buffer, int Width, int Height)
{
    if(Buffer->Memory)
    {
        VirtualFree(Buffer->Memory, 0, MEM_RELEASE);
    }
    
    Buffer->Width = Width;
    Buffer->Height = Height;
    
    int BytesPerPixel = 4;
    
    Buffer->Info.bmiHeader.biSize = sizeof(Buffer->Info.bmiHeader);
    Buffer->Info.bmiHeader.biWidth = Buffer->Width;
    Buffer->Info.bmiHeader.biHeight = -Buffer->Height;
    Buffer->Info.bmiHeader.biPlanes = 1;
    Buffer->Info.bmiHeader.biBitCount = 32;
    Buffer->Info.bmiHeader.biCompression = BI_RGB;
    
    int BitmapMemorySize = (Buffer->Width*Buffer->Height)*BytesPerPixel;
    Buffer->Memory = VirtualAlloc(0, BitmapMemorySize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    Buffer->Pitch = Width*BytesPerPixel;
}

internal void
Win32DisplayBufferInWindow(win32_offscreen_buffer *Buffer,
                           HDC DeviceContext, int WindowWidth, int WindowHeight)
{
    StretchDIBits(DeviceContext,
                  0, 0, WindowWidth, WindowHeight,
                  0, 0, Buffer->Width, Buffer->Height,
                  Buffer->Memory,
                  &Buffer->Info,
                  DIB_RGB_COLORS, SRCCOPY);
}

internal LRESULT CALLBACK
Win32MainWindowCallback(HWND Window,
                        UINT Message,
                        WPARAM WParam,
                        LPARAM LParam)
{       
    LRESULT Result = 0;
    switch(Message)
    {
        case WM_CLOSE:
        {
            GlobalRunning = false;
        } break;
        
        case WM_ACTIVATEAPP:
        {
            OutputDebugStringA("WM_ACTIVATEAPP\n");
        } break;
        
        case WM_DESTROY:
        {
            GlobalRunning = false;
        } break;
        
        case WM_SYSKEYDOWN:
        case WM_SYSKEYUP:
        case WM_KEYDOWN:
        case WM_KEYUP:
        case WM_PAINT:
        {
            PAINTSTRUCT Paint;
            HDC DeviceContext = BeginPaint(Window, &Paint);
            win32_window_dimension Dimension = Win32GetWindowDimension(Window);
            Win32DisplayBufferInWindow(&GlobalBackbuffer, DeviceContext,
                                       Dimension.Width, Dimension.Height);
            EndPaint(Window, &Paint);
        } break;
        
        default:
        {
            Result = DefWindowProc(Window, Message, WParam, LParam);
        } break;
    }
    
    return(Result);
}

internal void
Win32InitDirectSound(HWND Window, DWORD SampleRate, DWORD SecondaryBufferSize)
{
    LPDIRECTSOUND DSoundInterface;
    if(DS_OK == DirectSoundCreate(0, &DSoundInterface, 0))
    {
        OutputDebugStringA("DSoundCreate'\n");
        
        WAVEFORMATEX WaveFormat = {};
        WaveFormat.wFormatTag = WAVE_FORMAT_PCM;
        WaveFormat.nChannels = 2;
        WaveFormat.nSamplesPerSec = SampleRate;
        WaveFormat.wBitsPerSample = 16;
        WaveFormat.nBlockAlign = (WaveFormat.nChannels * WaveFormat.wBitsPerSample) / 8;
        WaveFormat.nAvgBytesPerSec = WaveFormat.nSamplesPerSec * WaveFormat.nBlockAlign;
        
        if(DS_OK == DSoundInterface->SetCooperativeLevel(Window, DSSCL_PRIORITY))
        {
            OutputDebugStringA("SetCooperative'\n");
            
            DSBUFFERDESC BufferInfo = {};
            BufferInfo.dwSize = sizeof(BufferInfo);
            BufferInfo.dwFlags = DSBCAPS_PRIMARYBUFFER;
            
            LPDIRECTSOUNDBUFFER PrimaryBuffer;
            if(DS_OK == DSoundInterface->CreateSoundBuffer(&BufferInfo, 
                                                           &PrimaryBuffer, 0))
            {
                OutputDebugStringA("CreatePrimary'\n");
                PrimaryBuffer->SetFormat(&WaveFormat);
            }
        }
        
        DSBUFFERDESC BufferInfo = {};
        BufferInfo.dwSize = sizeof(BufferInfo);
        BufferInfo.dwBufferBytes = SecondaryBufferSize;
        BufferInfo.lpwfxFormat = &WaveFormat;
        
        if(DS_OK == DSoundInterface->CreateSoundBuffer(&BufferInfo, 
                                                       &SecondaryBuffer, 0))
        {
            OutputDebugStringA("CreateSecondary'\n");
        }
    }
}

internal void *
ReadEntireFile(char *FileName)
{
    void *Result = 0;
    HANDLE FileHandle = CreateFile(FileName, 
                                   GENERIC_READ, 
                                   FILE_SHARE_READ, 
                                   0, 
                                   OPEN_EXISTING, 
                                   0,
                                   0);
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER FileSize;
        if(GetFileSizeEx(FileHandle, &FileSize))
        {
            Result = malloc(FileSize.QuadPart);
            ReadFile(FileHandle, Result, FileSize.QuadPart, 0, 0);
        }
    }
    
    return Result;
}

real32 North = (3.0f*PI)/2.0f;
real32 West = 2.0f*PI;
real32 South = PI/2.0f;
real32 East = PI;
real32 NorthWest = (7.0f*PI)/4;
real32 SouthWest = PI/4.0f;
real32 SouthEast = (3.0f*PI)/4.0f;
real32 NorthEast = (5.0f*PI)/4.0f;

enum
{
    RIFF_Chunk_ID = 0x46464952, 
    WAVE_ID = 0x45564157,
    fmt_Chunk_ID = 0x20746d66,  
    data_ChunkID = 0x61746164,  
};

internal void
LoadWAV(char *FileName, voice_info *Voice)
{
    WAVE_header *Header = (WAVE_header *)ReadEntireFile(FileName);
    assert(Header->ChunkID == RIFF_Chunk_ID && Header->Format == WAVE_ID);
    
    WAVE_fmt *SoundFormat = (WAVE_fmt *)(Header + 1);
    assert(SoundFormat->ChunkID == fmt_Chunk_ID);
    
    assert(SoundFormat->AudioFormat == 1);
    assert(SoundFormat->NumChannels == 2);
    assert(SoundFormat->SampleRate == 48000);
    assert(SoundFormat->BitsPerSample == 16);
    
    WAVE_data *SoundData = (WAVE_data *)(SoundFormat + 1);
    assert(SoundData->ChunkID == data_ChunkID);
    assert((SoundData->ChunkSize % 2) == 0);
    
    Voice->TotalBytes = SoundData->ChunkSize;
    Voice->Memory = ((int16 *)SoundData) + 4;
}

internal real32
clamp(real32 Source, real32 Min)
{
    real32 Result;
    
    if(Source > Min)
    {
        Result = Source; 
    }
    else
    {
        Result = Min;
    }
    return Result;
}

internal void
FillRegion(audio_buffer *AudioBuffer, int16 **SampleOut)
{
    *(*SampleOut)++ = 0; 
    *(*SampleOut)++ = 0;
    (*SampleOut)--; 
    (*SampleOut)--;
    
    for(int voice = 0;
        voice < 4;
        voice++)
    {
        if((AudioBuffer->Voices[voice].SampleIndex * AudioBuffer->BytesPerSample) 
           <  AudioBuffer->Voices[voice].TotalBytes)
        {
            if(voice > 0)
            {
                (*SampleOut)--; 
                (*SampleOut)--;
            }
            
#if 0           
            (*(*SampleOut)++) += 
                *AudioBuffer->Voices[voice].Memory++ * 
                (AudioBuffer->Voices[voice].Volume * AudioBuffer->Voices[voice].Panning);
            
            (*(*SampleOut)++) += 
                *AudioBuffer->Voices[voice].Memory++ *
                (AudioBuffer->Voices[voice].Volume * (1.0f - AudioBuffer->Voices[voice].Panning));            
#endif
                        
#if 1       // sin((1 - _crossValue)*HALF_PI)
            // sin(_crossValue*HALF_PI)
            // real32 Panning = AudioBuffer->Voices[voice].Panning;
            (*(*SampleOut)++) += 
                *AudioBuffer->Voices[voice].Memory++ * 
                (AudioBuffer->Voices[voice].Volume * 
                 sin((1.0f - AudioBuffer->Voices[voice].Panning) * (PI/2)));
            
            (*(*SampleOut)++) += 
                *AudioBuffer->Voices[voice].Memory++ *
                (AudioBuffer->Voices[voice].Volume * 
                 sin((AudioBuffer->Voices[voice].Panning) * (PI/2)));            
#endif
            
            AudioBuffer->Voices[voice].SampleIndex++;
        }
        else
        {
            if(!AudioBuffer->Voices[voice].OneShot)
            {
                AudioBuffer->Voices[voice].Memory = AudioBuffer->Voices[voice].FirstSample;
                AudioBuffer->Voices[voice].SampleIndex = 0;
            }
            else
            {
                
            }
        }
    } 
    AudioBuffer->RunningSampleIndex++;
}

internal void
Win32FillAudioBuffer(audio_buffer *AudioBuffer, 
                     DWORD ByteToLock, DWORD BytesToWrite)
{
    LPVOID Region1;
    LPVOID Region2;
    DWORD Region1Size;
    DWORD Region2Size;
    if(DS_OK == SecondaryBuffer->Lock(ByteToLock, BytesToWrite,
                                      &Region1, &Region1Size, 
                                      &Region2, &Region2Size, 0))
    {
        int32 Region1StepSize = Region1Size/AudioBuffer->BytesPerSample;
        int16 *SampleOut = (int16 *)Region1;
        for(int32 SampleIndex = 0;
            SampleIndex < Region1StepSize;
            ++SampleIndex)
        {
            FillRegion(AudioBuffer, &SampleOut);
        }
        
        int32 Region2StepSize = Region2Size/AudioBuffer->BytesPerSample;
        SampleOut = (int16 *)Region2;
        for(int32 SampleIndex = 0;
            SampleIndex < Region2StepSize;
            ++SampleIndex)
        {
            FillRegion(AudioBuffer, &SampleOut);
        }
    }
    SecondaryBuffer->Unlock(Region1, Region1Size, Region2, Region2Size);
}

inline int64
Win32GetCountsPerSecond(void)
{
    LARGE_INTEGER Result;
    QueryPerformanceFrequency(&Result);
    
    return Result.QuadPart;
}

inline int64
Win32GetCurrentCount(void)
{
    LARGE_INTEGER Result;
    QueryPerformanceCounter(&Result);
    
    return Result.QuadPart;
}

inline int64 
Win32GetCountsElapsed(int64 Start, int64 End)
{
    int64 Result = End - Start;
    return Result;
}

internal real32
MagToVol(int32 Mag, int32 Val)
{
    int32 Range = Val;
    real32 Result;
    Result = (real32)fabs(((double)Mag/(double)Range) - 1.0f);
    
    if(Mag <= Range)
    {
        Result = (real32)fabs(((double)Mag/(double)Range) - 1.0f);
    }
    else
    {
        Result = 0;
    }
    
    return Result;
}

int CALLBACK
WinMain(HINSTANCE Instance,
        HINSTANCE PrevInstance,
        LPSTR CommandLine,
        int ShowCode)
{
    bool32 TimeIsGranular = (timeBeginPeriod(1) == TIMERR_NOERROR);
    WNDCLASSA WindowClass = {};
    Win32ResizeDIBSection(&GlobalBackbuffer, 1280, 720);
    
    WindowClass.style = CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    WindowClass.lpfnWndProc = Win32MainWindowCallback;
    WindowClass.hInstance = Instance;
    WindowClass.lpszClassName = "MicrowaveWindowClass";
    
    if(RegisterClassA(&WindowClass))
    {
        HWND Window =
            CreateWindowExA(
            0,
            WindowClass.lpszClassName,
            "Microwave",
            WS_OVERLAPPEDWINDOW|WS_VISIBLE,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            0,
            0,
            Instance,
            0);
        if(Window)
        {
            HDC DeviceContext = GetDC(Window);
            SetCurrentDirectory("data/");
            
            int64 CountsPerSecond = Win32GetCountsPerSecond();
            int32 TargetFramesPerSecond = 30;
            int64 TargetCountsPerFrame = CountsPerSecond / (int64)TargetFramesPerSecond;
            real32 TargetMsPerFrame = 1000.0f / (real32)TargetFramesPerSecond;
            
            bool32 Toggle = false;
            
            rgb_color Green = {0, 165, 0};
            rgb_color Blue = {0, 165, 255};
            rgb_color Orange = {255, 165, 0};
            rgb_color Gray = {100, 100, 100};
            rgb_color Black = {20, 20 ,20};
            rgb_color white = {255, 244, 229};
            
            int32 HearingDistanceDry = 300;
            int32 HearingDistanceWet = 500;
                        
            v2 BodyInitPosition = {ScreenCenterX, ScreenCenterY};
            v2 BodyPosition = BodyInitPosition;
            v2 BodyVelocity  = {0};
            real32 BodyPolarR = 0.0f;
            real32 BodyPolarT = 0.0f;
            
            v2 NosePosition = BodyInitPosition;
            v2 NoseVelocity  = {0};
            real32 NosePolarR = 60.0f;
            real32 NosePolarT = West;
                        
            v2 Sound1Positon = {ScreenCenterX - 300.0f, ScreenCenterY};
            v2 Sound2Positon = {ScreenCenterX + 300.0f, ScreenCenterY};    
                
            real32 EarPolarR = 100.0f;
          
            v2 LeftEarPosition = BodyInitPosition;
            v2 LeftEarVelocity = {0};
            real32 LeftEarPolarT = East;
            
            v2 RightEarPosition = BodyInitPosition;
            v2 RightEarVelocity = {0};
            real32 RightEarPolarT = West;
            
            audio_buffer AudioBuffer = {0}; 
            AudioBuffer.SampleRate = 48000;
            AudioBuffer.BytesPerSample = sizeof(int16) * 2;                
            AudioBuffer.SecondaryBufferSize = 
                AudioBuffer.SampleRate * AudioBuffer.BytesPerSample;
            AudioBuffer.RunningSampleIndex = 0;
            AudioBuffer.LatencySampleCount = AudioBuffer.SampleRate / 8;                        
            AudioBuffer.MasterVolume = 1.0f;
            AudioBuffer.MasterPlaying = false;
            
            AudioBuffer.Voices[0] = {0};
            AudioBuffer.Voices[0].Playing = true;
            AudioBuffer.Voices[0].VolumeL = 1.0f;
            AudioBuffer.Voices[0].VolumeR = 1.0f;
            AudioBuffer.Voices[0].Volume = 1.0f;
            AudioBuffer.Voices[0].Panning = 0.5f;
            
            AudioBuffer.Voices[1] = {0};
            AudioBuffer.Voices[1].Playing = true;
            AudioBuffer.Voices[1].VolumeL = 1.0f;
            AudioBuffer.Voices[1].VolumeR = 1.0f;
            AudioBuffer.Voices[1].Volume = 1.0f;
            AudioBuffer.Voices[1].Panning = 0.5f;
            
            AudioBuffer.Voices[2] = {0};
            AudioBuffer.Voices[2].Playing = true;
            AudioBuffer.Voices[2].VolumeL = 1.0f;
            AudioBuffer.Voices[2].VolumeR = 1.0f;
            AudioBuffer.Voices[2].Volume = 1.0f;
            AudioBuffer.Voices[2].Panning = 0.5f;
            
            AudioBuffer.Voices[3] = {0};
            AudioBuffer.Voices[3].Playing = true;
            AudioBuffer.Voices[3].VolumeL = 1.0f;
            AudioBuffer.Voices[3].VolumeR = 1.0f;
            AudioBuffer.Voices[3].Volume = 1.0f;
            AudioBuffer.Voices[3].Panning = 0.5f;
            
            LoadWAV("room1piano.wav", &AudioBuffer.Voices[0]);
            LoadWAV("room2piano_rv.wav", &AudioBuffer.Voices[1]);
            LoadWAV("room1vuur.wav", &AudioBuffer.Voices[2]);
            LoadWAV("room2vuur_rv.wav", &AudioBuffer.Voices[3]);
            
            // should set this automatically somewhere else
            AudioBuffer.Voices[0].FirstSample = AudioBuffer.Voices[0].Memory;
            AudioBuffer.Voices[1].FirstSample = AudioBuffer.Voices[1].Memory;
            AudioBuffer.Voices[2].FirstSample = AudioBuffer.Voices[2].Memory;
            AudioBuffer.Voices[3].FirstSample = AudioBuffer.Voices[3].Memory;
            
            Win32InitDirectSound(Window, AudioBuffer.SampleRate, AudioBuffer.SecondaryBufferSize);
            SecondaryBuffer->Play(0, 0, DSBPLAY_LOOPING);
            
            int64 StartCounter = Win32GetCurrentCount();
            GlobalRunning = true;
            while(GlobalRunning)
            {
                MSG Message;
                while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
                {
                    if(Message.message == WM_QUIT)
                    {
                        GlobalRunning = false;
                    }
                    
                    TranslateMessage(&Message);
                    DispatchMessageA(&Message);
                }
                
                for (DWORD ControllerIndex = 0;
                     ControllerIndex < XUSER_MAX_COUNT;
                     ++ControllerIndex)
                {
                    XINPUT_STATE ControllerState;
                    if(XInputGetState(ControllerIndex, &ControllerState) == ERROR_SUCCESS)
                    {
                        XINPUT_GAMEPAD *Pad = &ControllerState.Gamepad;
                        
                        bool Up = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_UP);
                        bool Down = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
                        bool Left = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_LEFT);
                        bool Right = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
                        bool Start = (Pad->wButtons & XINPUT_GAMEPAD_START);
                        bool Back = (Pad->wButtons & XINPUT_GAMEPAD_BACK);
                        bool LeftShoulder = (Pad->wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
                        bool RightShoulder = (Pad->wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);
                        bool AButton = (Pad->wButtons & XINPUT_GAMEPAD_A);
                        bool BButton = (Pad->wButtons & XINPUT_GAMEPAD_B);
                        bool XButton = (Pad->wButtons & XINPUT_GAMEPAD_X);
                        bool YButton = (Pad->wButtons & XINPUT_GAMEPAD_Y);
                        
                        int16 StickRY = Pad->sThumbRY;
                        int16 StickRX = Pad->sThumbRX;
                        int16 StickLY = Pad->sThumbLY;
                        int16 StickLX = Pad->sThumbLX;

                        real32 LinearVelocity = 2.0f;
                        real32 LinearVelocityCorrect = LinearVelocity;
                        if(StickLX > 512*40 && StickLY > 512*40 )
                        {
                            BodyPolarR = LinearVelocity;
                            BodyPolarT = NorthWest;
                        }
                        else if(StickLX > 512*40 && StickLY <- 512*40 )
                        {
                            BodyPolarR = LinearVelocity;
                            BodyPolarT = SouthWest;
                        }
                        else if(StickLX < -512*40 && StickLY < -512*40 )
                        {
                            BodyPolarR = LinearVelocity;
                            BodyPolarT = SouthEast;
                        }
                        
                        else if(StickLX < -512*40 && StickLY > 512*40 )
                        {
                            BodyPolarR = LinearVelocity;
                            BodyPolarT = NorthEast;
                        }
                        else if(StickLY > 512*40)
                        {
                            BodyPolarR = LinearVelocityCorrect;
                            BodyPolarT = North;
                        }
                        else if(StickLY < -512*40)
                        {
                            BodyPolarR = LinearVelocityCorrect;
                            BodyPolarT = South;
                        }
                        else if(StickLX > 512*40)
                        {
                            BodyPolarR = LinearVelocityCorrect;
                            BodyPolarT = West;
                        }
                        else if(StickLX < -512*40)
                        {
                            BodyPolarR = LinearVelocityCorrect;
                            BodyPolarT = East;
                        }
                        else
                        {
                            BodyPolarR = 0.0f;
                        }
                        
                        real32 AngularVelocity = 0.05;
                        if(StickRX < -512*40)
                        {
                            LeftEarPolarT -= AngularVelocity;
                            RightEarPolarT -= AngularVelocity;
                        }
                        if(StickRX > 512*40)
                        {
                            LeftEarPolarT += AngularVelocity;
                            RightEarPolarT += AngularVelocity;
                        }

                        if(StickRY < -512*40)
                        {
                            EarPolarR--;
                        }
                        if(StickRY > 512*40)
                        {
                            EarPolarR++;                
                        }
                    }
                    else
                    {
                        // NOTE(casey): The controller is not available
                    }
                }
                
                DWORD BytesToWrite = 0;
                DWORD PlayCursor;
                if(DS_OK == SecondaryBuffer->GetCurrentPosition(&PlayCursor, 0))
                {
                    DWORD ByteToLock = 
                        (AudioBuffer.RunningSampleIndex * AudioBuffer.BytesPerSample) 
                        % AudioBuffer.SecondaryBufferSize;                
                    
                    DWORD TargetCursor =
                        (PlayCursor + (AudioBuffer.LatencySampleCount * AudioBuffer.BytesPerSample))
                        % AudioBuffer.SecondaryBufferSize;
                    
                    if(ByteToLock > TargetCursor)
                    {
                        BytesToWrite = AudioBuffer.SecondaryBufferSize - ByteToLock;
                        BytesToWrite += TargetCursor;
                    }
                    else if(ByteToLock < TargetCursor)
                    {
                        BytesToWrite = TargetCursor - ByteToLock;
                    }
                    else
                    {
                        BytesToWrite = TargetCursor - ByteToLock;
                    }
                    
                    Win32FillAudioBuffer(&AudioBuffer, ByteToLock, BytesToWrite);
                }
                
                v2 BodyDelta;
                v2 Horizon;
                int32 Magnitude;
                
                // update body 
                BodyVelocity = {BodyPolarR * cos(BodyPolarT), BodyPolarR * sin(BodyPolarT)}; 
                BodyPosition = Add(BodyPosition, BodyVelocity);
                NoseVelocity = {NosePolarR * cos(BodyPolarT), NosePolarR * sin(BodyPolarT)};
                NosePosition = Add(BodyPosition, NoseVelocity);
                LeftEarVelocity = {(EarPolarR * cos(LeftEarPolarT)), (EarPolarR * sin(LeftEarPolarT))};
                LeftEarPosition = Add(BodyPosition, LeftEarVelocity);
                RightEarVelocity = {(EarPolarR * cos(RightEarPolarT)), (EarPolarR * sin(RightEarPolarT))};
                RightEarPosition = Add(BodyPosition, RightEarVelocity);
                Horizon = Sub(BodyPosition, RightEarPosition);
                
                BodyDelta = Sub(BodyPosition, Sound1Positon);
                Magnitude = (int32)Mag(BodyDelta);
                AudioBuffer.Voices[0].Volume =  MagToVol(Magnitude, HearingDistanceDry);
                AudioBuffer.Voices[1].Volume =  MagToVol(Magnitude, HearingDistanceWet);               
                
                real32 ResultDot = Dot(Unit(Horizon), Unit(BodyDelta));
                AudioBuffer.Voices[0].Panning = acos(ResultDot) / PI;
                
                BodyDelta = Sub(BodyPosition, Sound2Positon);
                Magnitude = (int32)Mag(BodyDelta);
                AudioBuffer.Voices[2].Volume =  MagToVol(Magnitude, HearingDistanceDry);
                AudioBuffer.Voices[3].Volume =  MagToVol(Magnitude, HearingDistanceWet);                                            
                
                ResultDot = Dot(Unit(Horizon), Unit(BodyDelta));
                AudioBuffer.Voices[2].Panning = acos(ResultDot) / PI;
                 
#if 0
                // update volume based on distance
                v2 LeftEarDelta;
                v2 RightEarDelta;
                v2 BodyDelta;
                int32 Magnitude;
                
                LeftEarDelta = Sub(LeftEarPosition, Sound1Positon);
                RightEarDelta = Sub(RightEarPosition, Sound1Positon);
                Magnitude = (int32)Mag(LeftEarDelta);
                AudioBuffer.Voices[0].VolumeL =  MagToVol(Magnitude, HearingDistanceDry);
                Magnitude = (int32)Mag(RightEarDelta);
                AudioBuffer.Voices[0].VolumeR =  MagToVol(Magnitude, HearingDistanceDry);
                
                LeftEarDelta = Sub(LeftEarPosition, Sound2Positon);
                RightEarDelta = Sub(RightEarPosition, Sound2Positon);
                Magnitude = (int32)Mag(LeftEarDelta);
                AudioBuffer.Voices[2].VolumeL =  MagToVol(Magnitude, HearingDistanceDry);
                Magnitude = (int32)Mag(RightEarDelta);
                AudioBuffer.Voices[2].VolumeR =  MagToVol(Magnitude, HearingDistanceDry);
                
                BodyDelta = Sub(BodyPosition, Sound1Positon);
                Magnitude = (int32)Mag(BodyDelta);
                AudioBuffer.Voices[1].VolumeL =  MagToVol(Magnitude, HearingDistanceWet);
                AudioBuffer.Voices[1].VolumeR =  MagToVol(Magnitude, HearingDistanceWet);               
                
                BodyDelta = Sub(BodyPosition, Sound2Positon);
                Magnitude = (int32)Mag(BodyDelta);
                AudioBuffer.Voices[3].VolumeL =  MagToVol(Magnitude, HearingDistanceWet);
                AudioBuffer.Voices[3].VolumeR =  MagToVol(Magnitude, HearingDistanceWet);                                            
#endif
                // drawing and stuff
                ClearScreen(&GlobalBackbuffer, white);
                DrawRect(&GlobalBackbuffer, Black, 32, Sound1Positon);
                DrawRect(&GlobalBackbuffer, Black, 32, Sound2Positon);
                DrawRect(&GlobalBackbuffer, Green, 32, BodyPosition);
                DrawRect(&GlobalBackbuffer, Blue, 16, LeftEarPosition);
                DrawRect(&GlobalBackbuffer, Orange, 16, RightEarPosition);
                DrawRect(&GlobalBackbuffer, Green, 8, NosePosition);
                
                //DrawPoint(&GlobalBackbuffer, {255, 255, 255}, {640, 360});
                //DrawLine(&GlobalBackbuffer, {255, 255, 255}, BodyPosition, RightEarPosition);
                                
                win32_window_dimension Dimension = Win32GetWindowDimension(Window);
                
                int64 EndCounter = Win32GetCurrentCount();
                int64 CountsPerFrame = Win32GetCountsElapsed(StartCounter, EndCounter);
                
                if(CountsPerFrame < TargetCountsPerFrame)
                {
                    if(TimeIsGranular)
                    {
                        DWORD SleepMS = (DWORD)(((TargetCountsPerFrame - CountsPerFrame) * 1000) /
                                                CountsPerSecond);
                        if(SleepMS > 0)
                        {
                            Sleep(SleepMS - 0.001);
                        }
                    }
                    
                    int64 TestCountsPerFrame = Win32GetCountsElapsed(StartCounter, 
                                                                     Win32GetCurrentCount());
                    if(TestCountsPerFrame > TargetCountsPerFrame) 
                        OutputDebugStringA("Sleep to long\n"); 
                    
                    while(CountsPerFrame < TargetCountsPerFrame)
                    {
                        CountsPerFrame = Win32GetCountsElapsed(StartCounter, 
                                                               Win32GetCurrentCount());
                    }
                    
                }
                else
                {
                    // missed frame
                }
                
                CountsPerFrame = Win32GetCountsElapsed(StartCounter, Win32GetCurrentCount());
                StartCounter = EndCounter;
                
                Win32DisplayBufferInWindow(&GlobalBackbuffer, DeviceContext,
                                           Dimension.Width, Dimension.Height);
                
#if 0
                real64 MsPerFrame = ((real64)CountsPerFrame * 1000) / (real64)CountsPerSecond;
                real64 FPSCasey = (real64)CountsPerSecond / (real64)CountsPerFrame;
                real64 FPSTjom = 1000.0f/(MsPerFrame);
                
                char Buf[256];
                sprintf(Buf, "%.02fms, %.02ff/s, %.02ff/s\n", MsPerFrame, FPSCasey, FPSTjom);
                OutputDebugStringA(Buf);
#endif
            }
        }
        else
        {
            // TODO(Tjom): Logging
        }
    }
    else
    {
        // TODO(Tjom): Logging
    }
    
    return(0);
}
