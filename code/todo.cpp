/*
[v] figure out how to do one shots
[v] make codermicrowave go to project dir
[v] basic volume
[v] play multiple sounds on top of each other

[v] per voice volume
[v] per voice panning
	[v] research implementation of panning laws

[ ] drawing lines
[ ] drawing spheres

[ ] handle sound from behind
[ ] prevent walking through sound sources when using one ear.
[ ] minimalistic interface for manipulating the voices 
[ ] a way to switch between 'listening modes' for debugging  

[ ] -bug- sometimes upon start playback is slow
[ ] -bug- tend to the sporadic clicking bug (micro fades on start and stop?)
[ ] implement crossfades to prevent clicks on retriggers and stops

[ ] high/low-pass filters
[ ] simple delay
[ ] reverb filer
*/