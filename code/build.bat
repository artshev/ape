@echo off

IF NOT DEFINED clset (call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x64)
SET clset=64

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build	

cl -nologo -FC -Zi ..\code\microwave.cpp user32.lib gdi32.lib dsound.lib Winmm.lib xinput.lib

move microwave.exe ../microwave.exe 

popd
