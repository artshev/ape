struct v2
{
    real32 x;
    real32 y;
};

struct int32_v2
{
    int32 x;
    int32 y;
};

inline v2 
Add(v2 A, v2 B)
{
    v2 Result;
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;    
    return Result; 
}

inline v2
Sub(v2 A, v2 B)
{
    v2 Result;
    Result.x = A.x - B.x;
    Result.y = A.y - B.y;    
    return Result; 
}

inline real32
Dot(v2 A, v2 B)
{
    return (A.x * B.x) + (A.y * B.y);
}

inline real32
Mag(v2 A)
{
    return sqrt(Dot(A, A));
}

inline v2
Unit(v2 A)
{
    v2 Result;
    Result.x = A.x / Mag(A);
    Result.y = A.y / Mag(A);
    return Result;
}

inline bool32
Equal(v2 A, v2 B)
{
    return (A.x == B.x) && (A.y == B .y);
}


