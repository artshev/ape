# **APE** #

In it’s current state APE is simply a showcase of basic audio-related features in 2D space. 

# **How to build** #
Run included build.bat. If the build succeeds, the executable wil be located in the root directory of the project.

**IMPORTANT** To compile with the included batch file Visual Studio must be installed. 

# Have fun! #

#**Manual**#

on your xbox controller, use left thumb-stick to move the "listener", right thumb-stick to rotate "the head".

Note that ape makes use of **stereo panning**, that won't translate on monophonic playback devices.    this version **requires an xbox controller** to play.